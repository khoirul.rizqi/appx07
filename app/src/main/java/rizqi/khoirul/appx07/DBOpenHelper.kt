package rizqi.khoirul.appx07

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs = "create table mhs(nim text primary key, nama text, prodi text)"
        //val ins = "insert into mhs(nim,nama,prodi) values('1i10','slak','laskl')"
        db?.execSQL(tMhs)
        //db?.execSQL(ins)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "mahasiswa"
        val DB_Ver = 1
    }
}