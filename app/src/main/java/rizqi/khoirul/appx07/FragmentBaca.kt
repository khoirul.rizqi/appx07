package rizqi.khoirul.appx07

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_tugas.*
import kotlinx.android.synthetic.main.frag_data_tugas.view.*
import java.util.*

class FragmentBaca : Fragment(),View.OnClickListener {

    lateinit var thisParent : MainActivity
    lateinit var v : View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_tugas,container,false)
        v.btnPilih.setOnClickListener(this)
        return v
    }

    override fun onClick(v: View?) {
        val strToken = StringTokenizer(txInput.text.toString(),";",false)
        editText.setText(strToken.nextToken())
        editText2.setText(strToken.nextToken())
        editText3.setText(strToken.nextToken())
    }
}